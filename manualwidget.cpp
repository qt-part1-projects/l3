#include "manualwidget.h"


ManualWidget::ManualWidget(QWidget *parent) : QWidget(parent)
{
    QVBoxLayout* layout = new QVBoxLayout(this);

    QLabel *label = new QLabel(this);
    label->setText("Enter text");

    ledit= new QLineEdit(this);
    QPushButton* pbutton = new QPushButton(this);
    pbutton->setText("Ok");

    layout->addWidget(label);
    layout->addWidget(ledit);
    layout->addWidget(pbutton);

    setLayout(layout);

    connect(pbutton,SIGNAL(clicked(bool)),this,SLOT(showMessage()));
}

void ManualWidget::showMessage()
{
    QMessageBox::information(this,"Message","The text entered in \
                                    the manual widget window is:"+ledit->text() );

}

