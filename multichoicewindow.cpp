#include "multichoicewindow.h"
#include "ui_multichoicewindow.h"
#include "manualwidget.h"
#include "ui_designetwidget.h"

MultiChoiceWindow::MultiChoiceWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MultiChoiceWindow)
{
    ui->setupUi(this);
}

MultiChoiceWindow::~MultiChoiceWindow()
{
    delete ui;
}

void MultiChoiceWindow::on_pushButton_clicked()
{
    QWidget *w = 0;

    if(ui->radioManual->isChecked())
        w = new ManualWidget();
    else{
        w = new QWidget();
        Ui::DesignerWidget *dw1 = new Ui::DesignerWidget();
        dw1->setupUi(w);
    }

    if(ui->radioModal->isChecked())
        w->setWindowModality(Qt::ApplicationModal);

    w->setWindowTitle(ui->lineEdit->text());
    w->show();
}
