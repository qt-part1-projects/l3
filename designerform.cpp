#include "designerform.h"
#include "ui_designerform.h"

DesignerForm::DesignerForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DesignerForm)
{
    ui->setupUi(this);
}

DesignerForm::~DesignerForm()
{
    delete ui;
}

void DesignerForm::on_pushButton_clicked()
{
    qApp->exit();
}
