QT += widgets

HEADERS += \
    manualwidget.h \
    multichoicewindow.h \
    designerform.h

SOURCES += \
    manualwidget.cpp \
    main.cpp \
    multichoicewindow.cpp \
    designerform.cpp

FORMS += \
    designetwidget.ui \
    multichoicewindow.ui \
    designerform.ui
