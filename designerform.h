#ifndef DESIGNERFORM_H
#define DESIGNERFORM_H

#include <QWidget>

namespace Ui {
class DesignerForm;
}

class DesignerForm : public QWidget
{
    Q_OBJECT

public:
    explicit DesignerForm(QWidget *parent = 0);
    ~DesignerForm();

private slots:
    void on_pushButton_clicked();

private:
    Ui::DesignerForm *ui;
};

#endif // DESIGNERFORM_H
