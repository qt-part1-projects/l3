#ifndef MANUALWIDGET_H
#define MANUALWIDGET_H

#include <QWidget>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QHBoxLayout>
#include <QMessageBox>

class ManualWidget : public QWidget
{
    Q_OBJECT
    QLineEdit* ledit;

public:
    explicit ManualWidget(QWidget *parent = 0);

signals:

public slots:
    void showMessage();
};

#endif // MANUALWIDGET_H
